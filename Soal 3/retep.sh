#!/bin/bash

# Function to make sure password is correct
function check_password() {
    password=$1
    username=$2


    #Login Attempt
    if ! grep -q "^$username:$password$" ./users/users.txt; then
        echo "$(date '+%y/%m/%d %H:%M:%S') LOGIN: ERROR Failed login attempt on user $username" >> log.txt
        echo "Invalid username or password"
        exit 1
    fi
}

# Main function to read username and password
echo "Enter username:"
read username

echo "Enter password:"
read -s password
echo

check_password "$password" "$username"

# Echo to say if user successful login
echo "$(date '+%y/%m/%d %H:%M:%S') LOGIN: INFO User $username logged in" >> log.txt

echo "Welcome, $username!"
