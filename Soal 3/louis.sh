#!/bin/bash

# Requirements
# Password Length must be 8 Characters
# Password contains at least 1 uppercase, lowercase, and alphanumeric
# Password can't be the same as username
# Password can't be either chicken nor ernie

# Pseudocode
# Make function to check password and to check whether the user has already create the account or not
# Call every function in main 
# Then echo to print that their register has been successful

# Reference for upper and lower case -> https://linuxopsys.com/topics/convert-to-uppercase-or-lowercase-on-linux


# Function to check if password meets requirements above 
function check_password() {
    password=$1
    username=$2

    # Check password to make sure that it is bigger than 8
    if [ ${#password} -lt 8 ]; then
        echo "Password must be at least 8 characters long"
        exit 1
    fi

    # Check password to make sure it contains at least 1 uppercase, lowercase, and alphanumeric
    if ! echo "$password" | grep -q '[[:upper:]]' || ! echo "$password" | grep -q '[[:lower:]]' || ! echo "$password" | grep -q '[[:digit:]]'; then
        echo "Password must contain at least 1 uppercase letter, 1 lowercase letter and alphanumeric characters"
        exit 1
    fi

    # Check password to make sure that the password is not the same as the username
    if [ "$password" = "$username" ]; then
        echo "Password cannot be the same as username"
        exit 1
    fi

    # Check password to make sure it does not contain chicken nor ernie
    if echo "$password" | grep -qi 'chicken\|ernie'; then
        echo "Password cannot contain chicken or ernie"
        exit 1
    fi
}

# Check if the user has existed or not
function check_user_exists() {
    username=$1
    if grep -q "^$username:" ./users/users.txt; then
        echo "User already exists"
        exit 1
    fi
}

# Main function
echo "Enter username:"
read username

check_user_exists "$username"

echo "Enter password:"
read -s password
echo

check_password "$password" "$username"

echo "$username:$password" >> ./users/users.txt

# Print that the register has been successful
echo "$(date '+%y/%m/%d %H:%M:%S') REGISTER: INFO User $username registered successfully" >> log.txt
