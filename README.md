# sisop-praktikum-modul-1-2023-bj-u06


Group Members:
1. Eric Azka Nugroho [5025211064]
2. Kirana Alivia Enrico [5025211190]
3. Talitha Hayyinas Sahala [5025211263]



## Question 1

Bocchi hendak melakukan University Admission Test di Jepang. Bocchi ingin masuk ke universitas yang bagus. Akan tetapi, dia masih bingung sehingga ia memerlukan beberapa strategi untuk melakukan hal tersebut. Untung saja Bocchi menemukan file .csv yang berisi ranking universitas dunia untuk melakukan penataan strategi  : 

**A**
Bocchi ingin masuk ke universitas yang bagus di Jepang. Oleh karena itu, Bocchi perlu melakukan survey terlebih dahulu. Tampilkan 5 Universitas dengan ranking tertinggi di Jepang.

**B**
Karena Bocchi kurang percaya diri, coba cari Faculty Student Score(fsr score) yang paling rendah diantara 5 Universitas dari hasil filter poin a.

**C**
Karena Bocchi takut salah membaca ketika memasukkan nama universitas, cari 10 Universitas di Jepang dengan Employment Outcome Rank(ger rank) paling tinggi.

**D**
Bocchi ngefans berat dengan universitas paling keren di dunia. Bantu bocchi mencari universitas tersebut dengan kata kunci keren.

**CODE**

```
#!/bin/bash

#Soal A
echo "These are the Top 5 University in Japan based on 2023 QS World University Rankings:"
grep "Japan" soal.csv| sort -k1 -n| head -n5|
awk -F "," '{print $1, $2}'

#Soal B
echo "These are the Top 5 University that has lowest FSR Score based on 2023 QS World University Rankings:"
grep "Japan" soal.csv| sort -t "," -k9 -n| head -n5|
awk -F "," '{print $1, $2}'

#Soal C
echo "These are the Top 10 University that has the highest Employment Outcome:"
grep "Japan" soal.csv| sort -t "," -k20 -n| head -n10|
awk -F "," '{print $1, $2}'

#Soal D
echo "The 'Coolest' University:"
grep "Keren" soal.csv|
awk -F "," '{print $2}'

```

**Explaination**

_Soal A_

```
#Soal A
echo "These are the Top 5 University in Japan based on 2023 QS World University Rankings:"
grep "Japan" soal.csv| sort -k1 -n| head -n5|
awk -F "," '{print $1, $2}'
```

For Problem A, we were tasked to find the Top 5 University in Japan from the csv file. For the code itself, we use grep "Japan" soal.csv to find the list of university in Japan on the csv file. Then, to sort the university name and the rank, we use sort -k1 and -n to get the name and its rank. Finally, to get the top 5 university, we use head -n5 and print the column of rank and name of university by print $1 and $2.


_Soal B_

```
#Soal B
echo "These are the Top 5 University that has lowest FSR Score based on 2023 QS World University Rankings:"
grep "Japan" soal.csv| sort -t "," -k9 -n| head -n5|
awk -F "," '{print $1, $2}'
```
For Problem B, we were tasked to find the Top 5 University in Japan that has the lowest FSR Score. To solve this problem, we use grep "Japan" soal.csv to find the list of university. Then, to sort the frs score and each rank, we use sort -t "," -k9 -n. It will provide the university name by each frs score (-k9 because fsr is in column 9). To get the top 5 university, we used head -n5. Finally, to print the value, we use print $1 and $2 (For the rank and university name).


_Soal C_

```
#Soal C
echo "These are the Top 10 University that has the highest Employment Outcome:"
grep "Japan" soal.csv| sort -t "," -k20 -n| head -n10|
awk -F "," '{print $1, $2}'
```

For Problem C, we were tasked to find the top 10 university that has the highest employment outcome. Same as before, we use grep "Japan" soal.csv to find the data of university in Japan. Then, to sort the university, we use sort -t "," -k20 -n because the employment outcome is in column 20. To arrange the top 10, we use head -n10. To print the value, print $1 and $2. 


_Soal D_

```
#Soal D
echo "The 'Coolest' University:"
grep "Keren" soal.csv|
awk -F "," '{print $2}'
```

For Problem D, we were tasked to find the coolest university. To find it, we use grep "Keren" soal.csv to find the university that is labeled "Keren". Then, to print the name of the university, print $2. 



## Question 2

Kobeni ingin pergi ke negara terbaik di dunia bernama Indonesia. Akan tetapi karena uang Kobeni habis untuk beli headphone ATH-R70x, Kobeni tidak dapat melakukan hal tersebut. 


- Untuk melakukan coping, Kobeni mencoba menghibur diri sendiri dengan mendownload gambar tentang Indonesia. Coba buat script untuk mendownload gambar sebanyak X kali dengan X sebagai jam sekarang (ex: pukul 16:09 maka X nya adalah 16 dst. Apabila pukul 00:00 cukup download 1 gambar saja). Gambarnya didownload setiap 10 jam sekali mulai dari saat script dijalankan. Adapun ketentuan file dan folder yang akan dibuat adalah sebagai berikut:



- File yang didownload memilki format nama “perjalanan_NOMOR.FILE” Untuk NOMOR.FILE, adalah urutan file yang download (perjalanan_1, perjalanan_2, dst)



- File batch yang didownload akan dimasukkan ke dalam folder dengan format nama “kumpulan_NOMOR.FOLDER” dengan NOMOR.FOLDER adalah urutan folder saat dibuat (kumpulan_1, kumpulan_2, dst) 



- Karena Kobeni uangnya habis untuk reparasi mobil, ia harus berhemat tempat penyimpanan di komputernya. Kobeni harus melakukan zip setiap 1 hari dengan nama zip “devil_NOMOR ZIP” dengan NOMOR.ZIP adalah urutan folder saat dibuat (devil_1, devil_2, dst). Yang di ZIP hanyalah folder kumpulan dari soal di atas.

**CODE**

```
kobeni_liburan.sh

#!/bin/bash

# Requirements
# Write a script to download the image X times with X as the current hour which the images are downloaded every 10 hours starting from when the script is run
# The downloaded file has the name format "perjalanan_NOMOR.FILE" (ex.perjalanan_1, perjalanan_2, dst)
# The downloaded batch file will be put in a folder with the format name "kumpulan_NOMOR.FOLDER" (ex.kumpulan_1, kumpulan_2, dst)
# Do zip every 1 day with the zip name “devil_ZIP NUMBER” (ex. devil_1, devil_2, dst). The ZIP is just a collection folder of the questions above


#get the current hour and the hours is equal with the images 
hour=$(date +"%H")
num_photos=$hour

# make folder
while true
do
    folder_num=$(ls -d kumpulan_* 2>/dev/null | wc -l)
    folder_name="kumpulan_$((folder_num+1)).FOLDER"
    mkdir $folder_name
# download image
	for ((i=1;i<=$num_images;i++)); do
		wget -qO "$folder_name/perjalanan_$i.jpg" "https://source.unsplash.com/800x600/?indonesia"
	done
# make folder.zip
 if [[ $(date +"%H") -eq 00 ]]
    then
        zip -r "devil_$((folder_num+1)).zip" $folder_name
        rm -rf $folder_name
        num_images=1
    else
        num_images=0
  fi
	sleep 36000
done

# crontab -e
# 0 */10 * * * ls bash/home/Downloads/kobeni_liburan > /home/Downloads/list_files
# @hourly /home/Downloads/kobeni_liburan/kobeni_liburan.sh
# 0 0 * * * ls /home/Downloads/kobeni_liburan > /home/Downloads/list_files
# @daily /home/Downloads/kobeni_liburan/kobeni_liburan.sh

```
**Explaination**
In this problem, we were tasked to write a script to download the image X times with X as the current hour where it has certain requirements of the files and folders to be created, which the downloaded file has the name format "perjalanan_NOMOR.FILE" (ex.perjalanan_1, perjalanan_2, dst), 
The downloaded batch file will be put in a folder with the format name "kumpulan_NOMOR.FOLDER" (ex.kumpulan_1, kumpulan_2, dst), and Do zip every 1 day with the zip name “devil_ZIP NUMBER” (ex. devil_1, devil_2, dst). The ZIP is just a collection folder of the questions above. In this problem, we created a pseudocode to give us path in solving the problem. We created a function for get current hour to download the image, example if the current hour is "00:00" it will download 1 image only, else the system will download the image X times with X as the current hour, and also we create the downloaded batch file which will be put in a folder with the format name "kumpulan_NOMOR.FOLDER". After that we create a function for downloading the image and also the downloaded file which has the name format "perjalanan_NOMOR.FILE". Next, we have to create a function for make the zip file every 1 day with the zip name format “devil_ZIP NUMBER”. At last, we have to create crontjob to schedule tasks to run automatically at specific times, as in the task given we have to download image once every 10 hours and do a zip every 1 day.



## Question 3

Peter Griffin hendak membuat suatu sistem register pada script louis.sh dari setiap user yang berhasil didaftarkan di dalam file /users/users.txt. Peter Griffin juga membuat sistem login yang dibuat di script retep.sh

- Untuk memastikan password pada register dan login aman, maka ketika proses input passwordnya harus memiliki ketentuan berikut


1. Minimal 8 karakter
Memiliki minimal 1 huruf kapital dan 1 huruf kecil

1. Alphanumeric

1. Tidak boleh sama dengan username 

1. Tidak boleh menggunakan kata chicken atau ernie

- Setiap percobaan login dan register akan tercatat pada log.txt dengan format : YY/MM/DD hh:mm:ss MESSAGE. Message pada log akan berbeda tergantung aksi yang dilakukan user.

1. Ketika mencoba register dengan username yang sudah terdaftar, maka message pada log adalah REGISTER: ERROR User already exists

1. Ketika percobaan register berhasil, maka message pada log adalah REGISTER: INFO User USERNAME registered successfully

1. Ketika user mencoba login namun passwordnya salah, maka message pada log adalah LOGIN: ERROR Failed login attempt on user USERNAME

1. Ketika user berhasil login, maka message pada log adalah LOGIN: INFO User USERNAME logged in


**CODE**

louis.sh

```
#!/bin/bash

# Requirements
# Password Length must be 8 Characters
# Password contains at least 1 uppercase, lowercase, and alphanumeric
# Password can't be the same as username
# Password can't be either chicken nor ernie

# Pseudocode
# Make function to check password and to check whether the user has already create the account or not
# Call every function in main 
# Then echo to print that their register has been successful

# Reference for upper and lower case -> https://linuxopsys.com/topics/convert-to-uppercase-or-lowercase-on-linux


# Function to check if password meets requirements above 
function check_password() {
    password=$1
    username=$2

    # Check password to make sure that it is bigger than 8
    if [ ${#password} -lt 8 ]; then
        echo "Password must be at least 8 characters long"
        exit 1
    fi

    # Check password to make sure it contains at least 1 uppercase, lowercase, and alphanumeric
    if ! echo "$password" | grep -q '[[:upper:]]' || ! echo "$password" | grep -q '[[:lower:]]' || ! echo "$password" | grep -q '[[:digit:]]'; then
        echo "Password must contain at least 1 uppercase letter, 1 lowercase letter and alphanumeric characters"
        exit 1
    fi

    # Check password to make sure that the password is not the same as the username
    if [ "$password" = "$username" ]; then
        echo "Password cannot be the same as username"
        exit 1
    fi

    # Check password to make sure it does not contain chicken nor ernie
    if echo "$password" | grep -qi 'chicken\|ernie'; then
        echo "Password cannot contain chicken or ernie"
        exit 1
    fi
}

# Check if the user has existed or not
function check_user_exists() {
    username=$1
    if grep -q "^$username:" ./users/users.txt; then
        echo "User already exists"
        exit 1
    fi
}

# Main function
echo "Enter username:"
read username

check_user_exists "$username"

echo "Enter password:"
read -s password
echo

check_password "$password" "$username"

echo "$username:$password" >> ./users/users.txt

# Print that the register has been successful
echo "$(date '+%y/%m/%d %H:%M:%S') REGISTER: INFO User $username registered successfully" >> log.txt

```

**Explaination**

In this problem, we were tasked to create a user login system where it has certain requirements, which the password must contain 8 characters, 1 uppercase, lowercase, and alphanumeric, password can't be the same as username, and password can't be either chicken nor ernie. In this problem, we created a pseudocode to give us path in solving the problem. We created a function to check whether the username and password is correct using $1 and $2 (to make it easy to be called in the retep.sh file). As it is stated, we created 2 functions, which are function to check password requirements and to check whether password is the same as username or not. Then, we will call the function in main and print that the user has successfully register. In the function of checking the requirements, we seperate each requirements in an if statements. We use ```( if [ ${#password} -lt 8 ]; )``` to check whether the password length is bigger than 8, ```( if ! echo "$password" | grep -q '[[:upper:]]' || ! echo "$password" | grep -q '[[:lower:]]' || ! echo "$password" | grep -q '[[:digit:]]'; )``` to check whether the password contain a minimum of 1 uppercase, lowercase, and alphanumeric, ```( if [ "$password" = "$username" ]; )``` to check whether the password is not the same as the username, and ```( if echo "$password" | grep -qi 'chicken\|ernie' )``` to check password is not chicken nor ernie. Then, in the next function, we use ```( if grep -q "^$username:" ./users/users.txt; )``` to make sure that the username is not existed. Lastly, in the main function, we call every function to make sure that the account has fulfill every requirements and step. If the account has successfully been created, we will print the registration has been successful using the date and month also. It will be recorded in log.txt for the user registration, and users.txt for the username and password.


retep.sh

```
#!/bin/bash

# Function to make sure password is correct
function check_password() {
    password=$1
    username=$2


    #Login Attempt
    if ! grep -q "^$username:$password$" ./users/users.txt; then
        echo "$(date '+%y/%m/%d %H:%M:%S') LOGIN: ERROR Failed login attempt on user $username" >> log.txt
        echo "Invalid username or password"
        exit 1
    fi
}

# Main function to read username and password
echo "Enter username:"
read username

echo "Enter password:"
read -s password
echo

check_password "$password" "$username"

# Echo to say if user successful login
echo "$(date '+%y/%m/%d %H:%M:%S') LOGIN: INFO User $username logged in" >> log.txt

echo "Welcome, $username!"

```

**Explaination**

For the retep.sh file, we were asked to create a user login system that has been registered before from the louis.sh file. In this file, we will call the function to check whether the username and password is correct and the login attempt. In the username and password function, we will call it by using $1 and $2 from the previous file. Then, we will use ```( if ! grep -q "^$username:$password$" ./users/users.txt; )``` to make sure that the user has been created and recorded by users.txt. If not, it will print login error and invalid username or password. Then, in the main function, we will print that the user has successfully logged in.



## Question 4

Johan Liebert adalah orang yang sangat kalkulatif. Oleh karena itu ia mengharuskan dirinya untuk mencatat log system komputernya. File syslog tersebut harus memiliki ketentuan : 


- Backup file log system dengan format jam:menit tanggal:bulan:tahun (dalam format .txt).


- Isi file harus dienkripsi dengan string manipulation yang disesuaikan dengan jam dilakukannya backup seperti berikut:


- Menggunakan sistem cipher dengan contoh seperti berikut. Huruf b adalah alfabet kedua, sedangkan saat ini waktu menunjukkan pukul 12, sehingga huruf b diganti dengan huruf alfabet yang memiliki urutan ke 12+2 = 14


- Hasilnya huruf b menjadi huruf n karena huruf n adalah huruf ke empat belas, dan seterusnya.


- Setelah huruf z akan kembali ke huruf a


- Buat juga script untuk dekripsinya.


- Backup file syslog setiap 2 jam untuk dikumpulkan 😀.



for log_encrypt.sh
```
#!/bin/bash

#lowercase and uppercase
lower="abcdefghijklmnopqrstuvwxyz"
upper="ABCDEFGHIJKLMNOPQRSTUVWXYZ"

#current time and date
current_hour=$(date "+%H")
current_date=$(date "+%H:%M  %d:%m:%Y")

#backup file
backup_filename="/Home/Kirana/soal4/encrypted/${date}.txt"

#shift the alphabet
cat var/log/syslog | tr "${lower:0:26}${upper:0:26}" "${lower:${current_hour}:26}${upper:${current_hour}:26}" > "${backup_file}"

#cronjob command to automatic run every 2 hours
# 0 */2 * * * /bin/bash /home/Kirana/soal4/encrypted/log_encrypted.sh

```
**Explaination**
This bash script performs encryption on the contents of the system log file called "syslog". Here is the explanation of the code.

- The first few lines of the script define two variables 'lower' and 'upper' that store lowercase and uppercase alphabets.

-The script then retrieves the current hour and date using the 'date' command and stores them in the variables 'current_hour' and 'current_date'.

-The script defines a variable backup_filename that specifies the location and filename for the backup of the encrypted log file. The filename includes the current date and time.

-The tr command is used to perform the encryption. It reads the contents of the syslog file using cat var/log/syslog, and then replaces each character in the file with the character located current_hour positions ahead in the alphabet. For example, if the current hour is 5, then the letter 'a' will be replaced with the letter 'f', 'b' with 'g', and so on. The tr command uses the ```${lower:0:26}${upper:0:26}``` syntax to specify the set of characters to be replaced, which includes all the lowercase and uppercase letters of the alphabet.

-Finally, the script sets up a cron job to automatically run the encryption script every 2 hours.

for log_decrypt.sh

```
#!/bin/bash

mkdir -p  "syslog_decrypted"

read -p "insert encryypted syslog file name : " enc_file

lower="abcdefghijklmnopqrstuvwxyz"
upper="ABCDEFGHIJKLMNOPQRSTUVWXYZ"

hour_file=$(echo "$enc_file" | cut -d ':' -f1)

enc_path="/Home/Kirana/soal4/decrypted/syslog_decrypted/${enc_file}"

if[ -f "$enc_path"];then
cat"${enc_path}" | tr
"${lower:${hour_file}:26}${upper:${hour_file}:26}" "${lower:0:26}${upper:0:26} > $"{dec_path}"
fi


```
**Explanations**
This bash script open and decrypts a system log file that has been encrypted using a previous Bash script. Here's an explanation of the script

- Create a new direct named "syslog_decrypted" using the 'mkdir -p' command. The '-p' option is used to create parent directories if they do not exist.

- Defines the 'lower' and 'upper' variables that store the lowercase and uppercase alphabets.

- The script then uses the 'cut' command to extract the encryption hour from the file name. This encryption hour is used to return the alphabet to its original position when the encryption was performed in the previous script.

- The script defines the 'enc_path' variable which specifies the location and name of the descrypted log file.

- The script uses an 'if' statement to check if the decrypted log file already exists in the 'syslog_decrypted' directory. If it already exists, the script will open the file using 'cat' and decrypt it using the 'tr' command.

- The 'tr' command replaces each character in the file with the character that is 'hour_file' positions backwrds in the alphabet. For example, if the encryption hour is 5, then the letter 'f' will be replaced with 'a', 'g', with 'b', and so on. The 'tr' command uses the syntax ```${lower:${hour_file}:26}${upper:${hour_file}:26}``` to specify the set of characters that will be replaced, which includes all lowercase and uppercase letters of the alphabet.

- The decrypted result is displayed on the screen and then saved to the 'enc_path' file.
