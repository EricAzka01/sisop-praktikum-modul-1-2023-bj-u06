
#!/bin/bash

#Soal A
echo "These are the Top 5 University in Japan based on 2023 QS World University Rankings:"
grep "Japan" soal.csv| sort -k1 -n| head -n5|
awk -F "," '{print $1, $2}'

#Soal B
echo "These are the Top 5 University that has lowest FSR Score based on 2023 QS World University Rankings:"
grep "Japan" soal.csv| sort -t "," -k9 -n| head -n5|
awk -F "," '{print $1, $2}'

#Soal C
echo "These are the Top 10 University that has the highest Employment Outcome:"
grep "Japan" soal.csv| sort -t "," -k20 -n| head -n10|
awk -F "," '{print $1, $2}'

#Soal D
echo "The 'Coolest' University:"
grep "Keren" soal.csv|
awk -F "," '{print $2}'