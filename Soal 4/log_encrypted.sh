#!/bin/bash

#lowercase and uppercase
lower="abcdefghijklmnopqrstuvwxyz"
upper="ABCDEFGHIJKLMNOPQRSTUVWXYZ"

#current time and date
current_hour=$(date "+%H")
current_date=$(date "+%H:%M  %d:%m:%Y")

#backup file
backup_filename="/Home/Kirana/soal4/encrypted/${date}.txt"

#shift the alphabet
cat var/log/syslog | tr "${lower:0:26}${upper:0:26}" "${lower:${current_hour}:26}${upper:${current_hour}:26}" > "${backup_file}"

#cronjob command to automatic run every 2 hours
# 0 */2 * * * /bin/bash /home/Kirana/soal4/encrypted/log_encrypted.sh



